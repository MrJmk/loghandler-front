import { Injectable, Component } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {map} from  'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable()
export class WebApiService {

  private currentAccessToken:Promise<{value:string}>;
  private webApiUrl:string = "http://localhost:5552/api/";

  constructor(private _httpClient: HttpClient) {  }

  //#region public methods

  /** The Get Async method
   * @param controller The api controller
   * @param action The api action
   * @param params dictionary of parameters
   * @param token The access token
   * @returns deserialized object
   */
  public async GetAsync(controller:string, action:string, params:Map<string, string> = null, token:string=undefined):Promise<any> {
    var result = await this.GetAsyncHelper(this.FormatUrl(controller, action, params), token);
    return result;
  }

  /** The Delete Async method
   * @param controller The api controller
   * @param action The api action
   * @param params dictionary of parameters
   * @param token The access token
   * @returns deserialized object
   */
  public async DeleteAsync<T>(controller:string, action:string, params:Map<string, string> = null, token:string=null):Promise<T> {
    var result = await this.DeleteAsyncHelper(this.FormatUrl(controller, action, params), token);
    return result.pipe(map(response => <T>response));
  }

  /** The post async method
   * @param controller The api controller
   * @param action The api action
   * @param data The request as TRequest
   * @returns The deserialized object as Observable<TResponse>
   */
  public async PostFormJsonAsync<TRequest, TResponse>(controller:string, action:string, data:TRequest):Promise<Observable<TResponse>> {
    var header = new HttpHeaders();
    if(this.currentAccessToken!== null && (await this.currentAccessToken).value!==""){
      header.set("Authorization", "Bearer "+this.currentAccessToken);
    }
    if(data!==null){
      return this._httpClient.post(this.FormatUrl(controller,action), data,{headers:header})
        .pipe(map(response => <TResponse>response));
    } 
    return undefined;
  }

  /** The post async method
   * @param controller The api controller
   * @param action The api action
   * @param data The request as TRequest
   * @returns The deserialized object as Observable<TResponse>
   */
  public async PostFormUrlEncoded<TResponse>(controller:string, action:string, data:any):Promise<Observable<TResponse>> {
    if(data!==null){
      return this._httpClient.post(this.FormatUrl(controller,action), this.FormUrlEncodedContent(data),
        {headers:{"Content-Type": "application/x-www-form-urlencoded"}})
        .pipe(map(response => <TResponse>response));
    } 
    return undefined;
  }
  //#endregion

  //#region private methods
  
  /** format url
   * @param controller The controller name
   * @param action The action name
   * @param queryStrings dictionary of query strings
   * @returns Url params
   */
  private FormatUrl(controller:string, action:string, queryStrings:Map<string, string> = null):string{
    var param = "?";

    if (queryStrings !== null && queryStrings.size > 0){
      queryStrings.forEach((value: string, key: string, map: Map<string, string>) => param = param+key+"=" +value+"&"); 
      
      return this.webApiUrl+controller+"/"+action+"/"+param.slice(0, -1);
    }
    return this.webApiUrl+controller+"/"+action;
  }

  /** Form encoded content
   * @param element 
   * @param key 
   * @param list 
   */
  private FormUrlEncodedContent(element, key=undefined, list=undefined){
    var list = list || [];
    if(typeof(element)=='object'){
      for(var id in element){
        this.FormUrlEncodedContent(element[id], key?key+'['+id+']':id,list);
      }
    } else{
      list.push(key+'='+encodeURIComponent(element));
    }
    return list.join('&');
  }

  /** Get async helper
   * @param url The url
   * @param token The token
   * @returns The result coming from Web API
   */
  private async GetAsyncHelper(url:string, token:string=undefined):Promise<any> {
    var validToken:string;
    if(token && token.length>0 || this.currentAccessToken && (await this.currentAccessToken).value.length>0){
      validToken = (token && token.length>0) ? token : (await this.currentAccessToken).value;
      return this._httpClient.get(url, {headers:{"Authorization":"Bearer "+validToken}});
    }
    return this._httpClient.get(url);
  }

  /** Delete async helper
   * @param url The url
   * @param token The token
   * @returns The result coming from Web API
   */
  private async DeleteAsyncHelper(url:string, token:string=undefined):Promise<any> {
    if(token && token.length>0|| this.currentAccessToken && (await this.currentAccessToken).value.length>0){
      var validToken:string = (token && token.length>0) ? token : (await this.currentAccessToken).value;
      return this._httpClient.delete(url, { headers:{"Authorization":"Bearer "+validToken} });
    }
    return this._httpClient.delete(url);
  }
  //#endregion
}
