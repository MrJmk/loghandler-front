import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./Pages/home/home.module').then( m => m.HomeComponentModule)
  },
  {
    path: 'dashbord',
    loadChildren: () => import('./Pages/dashbord/dashbord.module').then( m => m.DashbordComponentModule)
  },
  {
    path: '', 
    redirectTo: 'home',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
