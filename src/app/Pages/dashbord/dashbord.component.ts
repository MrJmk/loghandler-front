import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import { ChartType, ChartOptions } from 'chart.js';
import { Label } from 'ng2-charts';
import { WebApiService } from 'src/app/Services/webApi/web-api.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-dashbord',
  templateUrl: './dashbord.component.html',
  styleUrls: ['./dashbord.component.scss'],
  providers:[WebApiService]
})
export class DashbordComponent implements OnInit {

  private ELEMENT_DATA: LogElement[] = [];

  /**
   * Chart Pie
   */
  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'right',
    },
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          return label;
        },
      },
    }
  };
  public pieChartLabels: Label[] = ['VERBOSE', 'HTTP', 'INFO', 'SILLY', 'ERROR'];
  public pieChartData: number[] = [0, 0, 0, 0, 0];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartColors = [
    {
      backgroundColor: ['rgba(116,192,190,0.7)', 'rgba(35,40,245,0.7)', 'rgba(0,125,255,0.7)', 'rgba(235,190,14,0.7)', 'rgba(255,0,0,0.7)'],
    },
  ];

  /**
   * Table
   */
  displayedColumns: string[] = ['level', 'message', 'date', 'file', 'position', 'server'];
  dataSource:any;

  constructor(private _webApi:WebApiService) { 
    //this.dataSource = new MatTableDataSource<LogElement>(this.ELEMENT_DATA);
    this.initListLog();
  }

  private async initListLog() {
    var result:Observable<any> = await this._webApi.GetAsync("listlog","index");
    result.subscribe((res:LogElement[]) =>{
        this.ELEMENT_DATA = res,
        this.dataSource = new MatTableDataSource<LogElement>(this.ELEMENT_DATA),
        this.initMethod()
      }, 
      e =>{console.log(e)});
  }

  private initMethod() {
    const verb:LogElement[] = this.ELEMENT_DATA.filter((value, index, array)=> value.level == "verbose");
    const deb:LogElement[] = this.ELEMENT_DATA.filter((value, index, array)=> value.level == "http");
    const inf:LogElement[] = this.ELEMENT_DATA.filter((value, index, array)=> value.level == "info");
    const war:LogElement[] = this.ELEMENT_DATA.filter((value, index, array)=> value.level == "silly");
    const err:LogElement[] = this.ELEMENT_DATA.filter((value, index, array)=> value.level == "error");
    this.pieChartData = [verb.length, deb.length, inf.length, war.length, err.length];
  }

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  ngOnInit() {
    setTimeout(() => this.dataSource.paginator = this.paginator);
    //this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
}

export interface LogElement {
  level: string;
  message:string;
  date: string;
  file: string;
  position:number
  server: string;
}
